package com.asoi.svintsitski;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class setOperationsTest {

    @Test
    public void union() {
        Set<Integer> firstSet = new HashSet<>(Arrays.asList(1, 2, 3));
        Set<Integer> secondSet = new HashSet<>(Arrays.asList(3, 4, 5, 6));
        Set<Integer> expected = new HashSet<>(Arrays.asList(1, 2, 3, 4, 5, 6));
        Set<Integer> actual = new HashSet<>(setOperations.union(firstSet, secondSet));
        assertEquals(expected, actual);
    }

    @Test
    public void intersection() {
        Set<Integer> firstSet = new HashSet<>(Arrays.asList(1, 2, 3));
        Set<Integer> secondSet = new HashSet<>(Arrays.asList(3, 4, 5, 6));
        Set<Integer> expected = new HashSet<>(Collections.singletonList(3));
        Set<Integer> actual = setOperations.intersection(firstSet, secondSet);
        assertEquals(expected, actual);
    }

    @org.junit.Test
    public void difference() {
        Set<Integer> firstSet = new HashSet<>(Arrays.asList(1, 2, 3));
        Set<Integer> secondSet = new HashSet<>(Arrays.asList(3, 4, 5, 6));
        Set<Integer> expected = new HashSet<>(Arrays.asList(1, 2));
        Set<Integer> actual = setOperations.difference(firstSet, secondSet);
        assertEquals(expected, actual);
    }

    @Test
    public void differenceNegative() {
        Set<Integer> firstSet = new HashSet<>();
        Set<Integer> secondSet = new HashSet<>(Arrays.asList(3, 4, 5, 6));
        Set<Integer> expected = new HashSet<>();
        Set<Integer> actual = setOperations.difference(firstSet, secondSet);
        assertEquals(expected, actual);
    }

    @Test
    public void exception() {
        Set<Integer> firstSet = new HashSet<>(Arrays.asList(1, 2, 3));
        Set<Integer> secondSet = new HashSet<>(Arrays.asList(3, 4, 5, 6));
        Set<Integer> expected = new HashSet<>(Arrays.asList(1, 2, 4, 5, 6));
        Set<Integer> actual = setOperations.exception(firstSet, secondSet);
        assertEquals(expected, actual);
    }
}