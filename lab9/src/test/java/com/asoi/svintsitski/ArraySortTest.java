package com.asoi.svintsitski;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class ArraySortTest {

    @Test
    public void sort() {
        int[] numbers = {1, 64, 100, 99, 14};
        int[] actual = ArraySort.Sort(numbers);
        int[] expected = {1, 100, 14, 64, 99};
        assertArrayEquals(expected, actual);
    }

    @Test
    public void sortNegative() {
        int[] numbers = {1, 64, -100, 99, 14};
        int[] actual = ArraySort.Sort(numbers);
        int[] expected = {1, -100, 14, 64, 99};
        assertArrayEquals(expected, actual);
    }
    @Test
    public void sortNegative2() {
        int[] numbers = {};
        int[] actual = ArraySort.Sort(numbers);
        int[] expected = {};
        assertArrayEquals(expected, actual);
    }
}