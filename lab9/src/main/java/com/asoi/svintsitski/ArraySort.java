package com.asoi.svintsitski;

import java.util.*;

final class ArraySort {
    static int[] Sort(int[] arr) {
        HashMap<Integer, Integer> sortMap = new HashMap<>();
        int count2 = 0;
        for (int element : arr) {
            int count = 0;
            String stringArr = String.valueOf(element);
            for (int j = 0; j < stringArr.length(); j++) {
                if (Character.isDigit(stringArr.charAt(j))) {
                    count += Integer.valueOf(String.valueOf(stringArr.charAt(j)));
                }
            }
            if (stringArr.length() != 0) {
                sortMap.put(count2, count);
            }
            count2++;
        }
        int[] result = new int[arr.length];
        sortMap = sortHashMapByValues(sortMap);
        int count = 0;
        for (Integer key : sortMap.keySet()) {
            result[count] = arr[key];
            count++;
        }
        return result;
    }

    private static LinkedHashMap<Integer, Integer> sortHashMapByValues(
            HashMap<Integer, Integer> passedMap) {
        List<Integer> mapKeys = new ArrayList<>(passedMap.keySet());
        List<Integer> mapValues = new ArrayList<>(passedMap.values());
        Collections.sort(mapValues);
        Collections.sort(mapKeys);

        LinkedHashMap<Integer, Integer> sortedMap =
                new LinkedHashMap<>();

        Iterator<Integer> valueIt = mapValues.iterator();
        while (valueIt.hasNext()) {
            Integer val = valueIt.next();
            Iterator<Integer> keyIt = mapKeys.iterator();

            while (keyIt.hasNext()) {
                Integer key = keyIt.next();
                Integer comp1 = passedMap.get(key);
                Integer comp2 = val;

                if (comp1.equals(comp2)) {
                    keyIt.remove();
                    sortedMap.put(key, val);
                    break;
                }
            }
        }
        return sortedMap;
    }
}