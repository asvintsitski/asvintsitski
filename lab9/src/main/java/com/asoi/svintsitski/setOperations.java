package com.asoi.svintsitski;

import java.util.HashSet;
import java.util.Set;

final class setOperations {
    static <T> Set<T> union(Set<T> a, Set<T> b) {
        Set<T> result = new HashSet<>(a);
        result.addAll(b);
        return result;
    }

    static <T> Set<T> intersection(Set<T> a, Set<T> b) {
        Set<T> result = new HashSet<>();
        for (T item : a) {
            for (T t : b) {
                if (t.equals(item)) {
                    result.add(item);
                }
            }
        }
        return result;
    }

    static <T> Set<T> difference(Set<T> a, Set<T> b) {
        Set<T> result = new HashSet<>();
        for (T t : a) {
            boolean coincidence = true;
            for (T t1 : b) {
                if (t1.equals(t)) {
                    coincidence = false;
                }
            }
            if (coincidence) {
                result.add(t);
            }
        }
        return result;
    }

    static <T> Set<T> exception(Set<T> a, Set<T> b) {
        Set<T> result = new HashSet<>();
        result.addAll(difference(a, b));
        result.addAll(difference(b, a));
        return result;
    }
}
