package com.asoi.svintsitski;

import java.util.*;

final class ConsoleOperation {

    private ConsoleOperation() {
    }

    static List<String> input() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите строку:");
        String[] inputString = scan.nextLine().split("[^0-9a-zA-Zа]+");
        return Arrays.asList(inputString);
    }

    static void print(Map<String, ArrayList<String>> map) {
        for (Map.Entry<String, ArrayList<String>> item : map.entrySet()) {
            System.out.print(item.getKey() + ": ");
            for (String x : item.getValue()) {
                System.out.println("\t" + x);
            }
        }
    }
}
