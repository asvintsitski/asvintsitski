package com.asoi.svintsitski;

import java.util.*;

final class Structure {

    private Structure(){

    }

    static Map createMap(List<String> itemList) {
        Map<String, ArrayList<String>> struct = new HashMap<>();
        for (String x : itemList) {
            if (!x.equals("")) {
                String key = String.valueOf(x.charAt(0)).toUpperCase();
                ArrayList<String> arr;
                if (struct.get(key) == null) {
                    arr = remakeMap(new ArrayList<>(), x);
                } else {
                    arr = remakeMap(struct.get(key), x);
                }
                struct.put(key, arr);
            }
        }
        return struct;
    }

    private static ArrayList<String> remakeMap(ArrayList<String> arr, String newWord) {
        ArrayList<String> struct = new ArrayList<>(arr);
        struct.add(newWord.toLowerCase());
        Collections.sort(struct);
        return struct;
    }

    static ArrayList<String> countWord(Map<String, ArrayList<String>> map) {
        ArrayList<String> arrayCountWord = new ArrayList<>();
        for (Map.Entry<String, ArrayList<String>> item : map.entrySet()) {
            ArrayList<String> list = item.getValue();
            Set<String> set = new HashSet<>(list);
            for (String r : set) {
                String newWordCount = r + " " + Collections.frequency(list, r);
                arrayCountWord.add(newWordCount);
            }
        }
        return arrayCountWord;
    }
}
