package com.asoi.svintsitski;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import static com.asoi.svintsitski.ConsoleOperation.print;
import static com.asoi.svintsitski.Structure.countWord;
import static com.asoi.svintsitski.Structure.createMap;

public class Main {
    public static void main(String[] args) {
        List<String> itemList = ConsoleOperation.input();
        Map map = createMap(itemList);
        print(new TreeMap<>(createMap(countWord(map))));
    }
}
