package com.asoi.svintsitski;

import java.util.*;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class StructureTest {

    @org.junit.Test
    public void createMap() {
        List<String> listStart = Arrays.asList("fee", "fi", "foe", "da", "foe");
        Map listRes = Structure.createMap(listStart);
        Map<String, ArrayList<String>> listTest = new HashMap<>();
        ArrayList<String> fTest = new ArrayList<>(Arrays.asList("fee", "fi", "foe", "foe"));
        listTest.put("F", fTest);
        listTest.put("D", new ArrayList<>(Collections.singletonList("da")));
        assertThat(listRes, is(listTest));
    }

    @org.junit.Test
    public void countWord() {
        List<String> listStart = Arrays.asList("fee", "fi", "foe", "da", "foe", "3asd", "1aa");
        ArrayList<String> arrTest = new ArrayList<>(Arrays.asList("1aa 1", "3asd 1", "da 1", "fi 1", "fee 1", "foe 2"));
        ArrayList arrRes = Structure.countWord(Structure.createMap(listStart));
        assertThat(arrRes, is(arrTest));
    }

}