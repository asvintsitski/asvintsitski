package com.asoi.svintsitski;

import com.asoi.svintsitski.exception.IntegerException;
import com.asoi.svintsitski.exception.StringException;
import com.asoi.svintsitski.validator.ValidatorSystem;
import org.junit.Assert;
import org.junit.Test;

public class MainTest {
    private ValidatorSystem validatorSystem = new ValidatorSystem();
    private String result;
    private Object object;
    private final char quote = (char) 34;

    @Test
    public void testValidateInt() throws IntegerException, StringException {
        object = 3;
        result = "Verification passed successfully. " + quote + "" + object + "" + quote + " is Integer.";
        Assert.assertEquals(validatorSystem.validate(object), result);
    }

    @Test
    public void testValidateIntFails() throws IntegerException, StringException {
        object = 0;
        result = "Error. It is Integer. The " + quote + "" + object + "" + quote + " is less than 1";
        Assert.assertEquals(validatorSystem.validate(object), result);
    }

    @Test
    public void testValidateIntFails2() throws IntegerException, StringException {
        object = 11;
        result = "Error. It is Integer. The " + quote + "" + object + "" + quote + " is more than 10";
        Assert.assertEquals(validatorSystem.validate(object), result);
    }

    @Test
    public void testValidateString() throws IntegerException, StringException {
        object = "Wad";
        result = "Verification passed successfully. " + "" + quote + object + quote + " is String.";
        Assert.assertEquals(validatorSystem.validate(object), result);
    }

    @Test
    public void testValidateStringFails() throws IntegerException, StringException {
        object = "wad";
        result = "Error. " + quote + "" + object + "" + quote + " is String. First character must be uppercase";
        Assert.assertEquals(validatorSystem.validate(object), result);
    }

    @Test
    public void testValidateStringFails2() throws IntegerException, StringException {
        object = "";
        result = "Error. It is String. String cannot be empty";
        Assert.assertEquals(validatorSystem.validate(object), result);
    }

    @Test
    public void testValidateCharFails() throws IntegerException, StringException {
        object = 'a';
        result = "Неизвестный объект";
        Assert.assertEquals(validatorSystem.validate(object), result);
    }
}