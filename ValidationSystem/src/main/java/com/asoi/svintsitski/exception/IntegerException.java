package com.asoi.svintsitski.exception;

public class IntegerException extends Exception {
    private String message;

    public String getMessage() {
        return message;
    }

    public IntegerException(String message) {
        this.message = message;
    }
}
