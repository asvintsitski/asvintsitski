package com.asoi.svintsitski.exception;

public class StringException extends Exception {
    private String message;

    public String getMessage() {
        return message;
    }

    public StringException(String message) {
        this.message = message;
    }
}
