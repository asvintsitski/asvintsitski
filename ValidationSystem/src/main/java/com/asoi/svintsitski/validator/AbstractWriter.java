package com.asoi.svintsitski.validator;

import com.asoi.svintsitski.exception.IntegerException;
import com.asoi.svintsitski.exception.StringException;

public abstract class AbstractWriter {
    public abstract String write() throws IntegerException, StringException;
}