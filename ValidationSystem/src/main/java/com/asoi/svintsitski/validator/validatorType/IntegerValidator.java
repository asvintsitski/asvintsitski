package com.asoi.svintsitski.validator.validatorType;

import com.asoi.svintsitski.exception.IntegerException;
import com.asoi.svintsitski.validator.AbstractWriter;

public class IntegerValidator extends AbstractWriter {
    private Integer content;

    public IntegerValidator(Object object) {
        content = (Integer) object;
    }

    public String write() {
        try {
            final char quote = (char) 34;
            if (content < 1) {
                throw new IntegerException("It is Integer. The " + quote + "" + content + "" + quote + " is less than 1");
            }
            if (content > 10) {
                throw new IntegerException("It is Integer. The " + quote + "" + content + "" + quote + " is more than 10");
            }
            return "Verification passed successfully. " + quote + "" + content + "" + quote + " is Integer.";
        } catch (IntegerException e) {
            return "Error. " + e.getMessage();
        }
    }
}
