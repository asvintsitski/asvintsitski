package com.asoi.svintsitski.validator.validatorType;

import com.asoi.svintsitski.exception.StringException;
import com.asoi.svintsitski.validator.AbstractWriter;

public class StringValidator extends AbstractWriter {
    private String content;

    public StringValidator(Object object) {
        content = (String) object;
    }

    public String write() {
        try {
            final char quote = (char) 34;
            String[] splitString = content.trim().split("(?<=[\\S])[\\S]+");
            if (splitString[0].equals("")) {
                throw new StringException("It is String. String cannot be empty");
            }
            if (!splitString[0].equals(splitString[0].toUpperCase())) {
                throw new StringException(quote + "" + content + "" + quote + " is String. First character must be uppercase");
            }
            return "Verification passed successfully. " + quote + content + quote + " is String.";
        } catch (StringException e) {
            return "Error. " + e.getMessage();
        }
    }
}