package com.asoi.svintsitski.validator;

import com.asoi.svintsitski.exception.IntegerException;
import com.asoi.svintsitski.exception.StringException;
import com.asoi.svintsitski.validator.validatorType.IntegerValidator;
import com.asoi.svintsitski.validator.validatorType.StringValidator;

import java.util.Objects;

public class ValidatorSystem<T> {
    public String validate(T object) throws IntegerException, StringException {
        AbstractWriter writer = (object instanceof String) ? new StringValidator(object) : null;
        writer = (!(object instanceof Integer)) ? writer : new IntegerValidator(object);
        if (writer != null) {
            System.out.println(Objects.requireNonNull(writer).write());
            return Objects.requireNonNull(writer).write();
        }
        return "Неизвестный объект";
    }
}
