package com.asoi.svintsitski.validator;

import com.asoi.svintsitski.exception.IntegerException;
import com.asoi.svintsitski.exception.StringException;

public class Main {
    public static void main(String[] args) throws IntegerException, StringException {
        ValidatorSystem validatorSystem = new ValidatorSystem();
        validatorSystem.validate("wad");
        validatorSystem.validate(123);
        validatorSystem.validate("Ad");
        validatorSystem.validate(3);
        validatorSystem.validate("");
        validatorSystem.validate('f');
    }
}
