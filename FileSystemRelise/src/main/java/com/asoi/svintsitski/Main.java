package com.asoi.svintsitski;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        do {
            System.out.println("Главное меню:\n1.Добавить путь к файлу/каталогу\n2.Отобразить структуру\n3.Очистить структуру\n4.Выход");
            switch (scan.nextLine()) {
                case ("1"):
                    InputString.SplitString(scan.nextLine());
                    ConsoleClear(scan);
                    break;
                case ("2"):
                    Main.StructVisible(scan);
                    break;
                case ("3"):
                    Main.StructClear(scan);
                    break;
                case ("4"):
                    System.exit(0);
                    break;
                default:
                    System.out.println("Повторите попытку.");
                    continue;
            }
        } while (true);
    }

    public static void StructClear(Scanner scan) {
        WorkFile.WriteFile(new StringBuffer());
        ConsoleClear(scan);
    }

    public static void StructVisible(Scanner scan) {
        System.out.println(WorkFile.ReadFile());
        ConsoleClear(scan);
    }

    public static void ConsoleClear(Scanner scan) {
        System.out.println("Для продолжения нажмите Enter");
        scan.nextLine();
        for (int i = 0; i < 10; ++i) System.out.println();
    }
}
