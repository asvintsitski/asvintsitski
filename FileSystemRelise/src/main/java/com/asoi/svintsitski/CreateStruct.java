package com.asoi.svintsitski;

import java.util.ArrayList;
import java.util.List;

public class CreateStruct {
    public static void write(String text) {
        StringBuffer sb = new StringBuffer(WorkFile.ReadFile());
        String writestr = "";
        List<String> myList = new ArrayList<String>();
        for (String retval : text.split("\n")) {
            myList.add(retval);
        }
        boolean stop = true;
        int startindex0;
        int startindex1 = 0;
        int endindex0 = 0;
        int endindex1 = sb.length();
        String generate = "";
        for (Object list : myList) {
            generate = "";
            startindex0 = 0;
            endindex0 = 0;
            for (int j = startindex0; j < endindex1; j++) {
                if (sb.charAt(j) == '\n') {
                    startindex0 += 1;
                    if (generate.equals(String.valueOf(list)) && startindex1 <= startindex0) {
                        break;
                    }
                    generate = "";
                    continue;
                }
                generate += sb.charAt(j);
                startindex0 += 1;
            }
            for (int j = 0; j < endindex1; j++) {
                if (sb.charAt(j) == '\n') {
                    endindex0 += 1;
                    if (generate.equals(String.valueOf(list)) && endindex0 <= endindex1 && endindex0 >= startindex0) {
                        break;
                    }
                    generate = "";
                    continue;
                }
                generate += sb.charAt(j);
                endindex0 += 1;
            }
            startindex1 = startindex0;
            endindex1 = endindex0;
            if (sb.lastIndexOf(String.valueOf(list)) != -1 && stop) {
                stop = true;
            } else {
                stop = false;
                writestr += list + "\n";
            }
        }
        if (endindex0 == sb.length() - 1) endindex0 += 1;

        if (writestr.length() != 0) {
            sb.insert(endindex0, writestr);
            WorkFile.WriteFile(sb);
        } else {
            System.out.println("Путь уже указан");
        }
    }
}
