package com.asoi.svintsitski;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class WorkFile {
    public static void WriteFile(StringBuffer sb) {
        try (FileWriter writer = new FileWriter("fileStruct.txt", false)) {
            writer.write(String.valueOf(sb));
            writer.flush();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static String ReadFile() {
        String saveStruct = "";
        try (FileReader reader = new FileReader("fileStruct.txt")) {
            int c;
            while ((c = reader.read()) != -1) {
                saveStruct += (char) c;
            }
        } catch (IOException ex) {

            System.out.println(ex.getMessage());
        }
        return saveStruct;
    }
}
