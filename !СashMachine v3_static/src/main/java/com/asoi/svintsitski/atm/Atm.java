package com.asoi.svintsitski.atm;

public class Atm {
    Card card;

    public Atm(Card card) {
        this.card = card;
    }

    public double balance() {
        return this.card.displayBalance();
    }

    public double refill(double sum) {
        return this.card.refill(sum);
    }

    public double balanceReduction(double sum) {
        return this.card.balanceReduction(sum);
    }

}
