package com.asoi.svintsitski.atm;

public class CreditCard extends Card {

    CreditCard(String name) {
        super(name, 0);
    }

    public CreditCard(String name, double balance) {
        super(name, balance);
    }

    @Override
    public double balanceReduction(double exchange) {
        balance -= exchange;
        return balance;
    }
}
