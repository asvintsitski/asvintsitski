package com.asoi.svintsitski.atm;

public class DebitCard extends Card {

    DebitCard(String name) {
        super(name, 0);
    }

    public DebitCard(String name, double balance) {
        super(name, (balance < 0) ? 0 : balance);
}

}
