package com.asoi.svintsitski.sort;

public class SortingContext {
    public static void main(String[] args) {
        execute();
    }

    public static void execute() {
        BubbleSort bs = new BubbleSort();
        SortBySelection sbs = new SortBySelection();
        bs.sort(new int[]{12, 55, 4, 69, 2, 7, 5, 88, 44, 15, 55});
        sbs.sort(new int[]{12, 55, 4, 69, 2, 7, 5, 88, 44, 15, 55});
    }
}
