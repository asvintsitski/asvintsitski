package com.asoi.svintsitski.sort;

public interface Sorter {
    void sort(int[] arr);
}