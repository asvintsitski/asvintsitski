package com.asoi.svintsitski.sort;

public class BubbleSort implements Sorter {

    public void sort(int[] arr) {
        System.out.print("Сортировка пузырьком: \t");
        bubbleSorter(arr);
        Printer.printer(arr);
    }

    private int[] bubbleSorter(int[] arr) {
        for (int out = arr.length - 1; out >= 1; out--) {
            for (int in = 0; in < out; in++) {
                if (arr[in] > arr[in + 1])
                    arr = toSwap(arr, in, in + 1);
            }
        }
        return arr;
    }

    private int[] toSwap(int[] arr, int first, int second) {
        int dummy = arr[first];
        arr[first] = arr[second];
        arr[second] = dummy;
        return arr;
    }
}