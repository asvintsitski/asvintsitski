package com.asoi.svintsitski.sortTest;

import com.asoi.svintsitski.atm.Card;
import org.junit.Assert;
import org.junit.Test;

public class CardTest {
    Card artem_svintitsky = new Card("Artem Svintitsky", 500);

    @Test
    public void testBalance() {
        double result = artem_svintitsky.displayBalance();
        Assert.assertEquals(500, result, 0);
    }

    @Test
    public void testExchange() {
        double result = artem_svintitsky.exchange(2);
        Assert.assertEquals(250, result, 0);
    }

    @Test
    public void testBalanceReduction() {
        double result = artem_svintitsky.balanceReduction(105.10);
        Assert.assertEquals(394.9, result, 0);
    }

    @Test
    public void testRefull() {
        double result = artem_svintitsky.refill(205.20);
        Assert.assertEquals(705.2, result, 0);
    }

}
