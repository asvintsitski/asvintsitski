package com.asoi.svintsitski.sortTest;

import com.asoi.svintsitski.sort.BubbleSort;
import com.asoi.svintsitski.sort.SortBySelection;
import com.asoi.svintsitski.sort.Sorter;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class SortingContextTest {
    int[] array = new int[]{1, 3, 2};
    Sorter sort = new BubbleSort();
    Sorter sbs = new SortBySelection();

    @Test
    public void BubbleSortTest() {
        sort.sort(array);
        assertArrayEquals(new int[]{1, 2, 3}, array);
    }

    @Test
    public void SortBySelectionTest() {
        sbs.sort(array);
        assertArrayEquals(new int[]{1, 2, 3}, array);
    }
}