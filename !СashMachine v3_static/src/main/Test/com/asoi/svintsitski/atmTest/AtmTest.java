package com.asoi.svintsitski.atmTest;

import com.asoi.svintsitski.atm.Atm;
import com.asoi.svintsitski.atm.CreditCard;
import com.asoi.svintsitski.atm.DebitCard;
import org.junit.Assert;

public class AtmTest {
    Atm atm = new Atm(new CreditCard("svin", 3000));

    @org.junit.Test
    public void balance() {
        Assert.assertEquals(3000, atm.balance(), 0);
        atm = new Atm(new DebitCard("svin", -3000));
        Assert.assertEquals(0, atm.balance(), 0);
    }

    @org.junit.Test
    public void refull() {
        atm = new Atm(new DebitCard("svin", 3000));
        Assert.assertEquals(3500, atm.refill(500), 0);
        atm = new Atm(new CreditCard("svin", 3000));
        Assert.assertEquals(3500, atm.refill(500), 0);

    }

    @org.junit.Test
    public void reductionBalance() {
        atm = new Atm(new DebitCard("svin", 3000));
        Assert.assertEquals(2500, atm.balanceReduction(500), 0);
        Assert.assertEquals(0, atm.balanceReduction(50000000), 0);
        atm = new Atm(new CreditCard("svin", 3000));
        Assert.assertEquals(-500, atm.balanceReduction(3500), 0);

    }
}