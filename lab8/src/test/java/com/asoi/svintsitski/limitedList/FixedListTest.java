package com.asoi.svintsitski.limitedList;

import org.junit.Assert;
import org.junit.Test;

public class FixedListTest {

    @Test
    public void add() throws FixedListException {
        FixedList list1 = new FixedList(1);
        list1.add(123);
        int value = 123;
        Assert.assertEquals(list1.get(0), value);
    }

    @Test
    public void get() throws FixedListException {
        FixedList list = new FixedList(3);
        list.add(1);
        list.add(2);
        list.add(3);
        int value = 2;
        Assert.assertEquals(list.get(1), value);
    }

    @Test
    public void remove() throws FixedListException {
        FixedList list = new FixedList(3);
        list.add(1);
        list.remove(1);
        int size = 0;
        Assert.assertEquals(list.size(), size);
    }

    @Test
    public void size() {
        FixedList list = new FixedList(1);
        int size = 0;
        Assert.assertEquals(list.size(), size);
    }

    @Test
    public void resize() throws FixedListException {
        FixedList list = new FixedList(2);
        list.add("1");
        list.add(2);
        list.resize(3);
        list.add(2);
        int size = 3;
        Assert.assertEquals(list.size(), size);
    }

}