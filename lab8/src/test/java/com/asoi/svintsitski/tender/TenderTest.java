package com.asoi.svintsitski.tender;

import org.junit.Assert;
import org.junit.Test;

public class TenderTest {

    @Test
    public void result() {
        Tender library = new Tender();
        library.addSkill(Skills.ARCHITECT, 1);
        library.addSkill(Skills.ENGINEER, 1);
        library.addSkill(Skills.PAINTER, 1);
        Team team1 = new Team("Team 1");
        team1.addWorker(new Worker(13F, Skills.ENGINEER, Skills.ARCHITECT));
        team1.addWorker(new Worker(13F, Skills.ENGINEER, Skills.ARCHITECT));
        team1.addWorker(new Worker(2.6F, Skills.PAINTER, Skills.ENGINEER, Skills.ARCHITECT));
        library.addTeam(team1);
        Team team2 = new Team("Team 2");
        team2.addWorker(new Worker(29F, Skills.ENGINEER, Skills.ARCHITECT, Skills.PAINTER));
        library.addTeam(team2);
        Team team3 = new Team("Team 3");
        team3.addWorker(new Worker(1F, Skills.ENGINEER, Skills.ARCHITECT));
        library.addTeam(team3);
        String result = "Самая дешевая бригада: Team 1. Ее цена: 28.6.";
        Assert.assertEquals(library.result(), result);
    }

    @Test
    public void resultNegative() {
        Tender library = new Tender();
        library.addSkill(Skills.ARCHITECT, 1);
        library.addSkill(Skills.ENGINEER, 5);
        library.addSkill(Skills.PAINTER, 1);
        Team team1 = new Team("Team 1");
        team1.addWorker(new Worker(13F, Skills.ENGINEER, Skills.ARCHITECT));
        team1.addWorker(new Worker(13F, Skills.ENGINEER, Skills.ARCHITECT));
        team1.addWorker(new Worker(2.6F, Skills.PAINTER, Skills.ENGINEER, Skills.ARCHITECT));
        library.addTeam(team1);
        Team team2 = new Team("Team 2");
        team2.addWorker(new Worker(29F, Skills.ENGINEER, Skills.ARCHITECT, Skills.PAINTER));
        library.addTeam(team2);
        Team team3 = new Team("Team 3");
        team3.addWorker(new Worker(1F, Skills.ENGINEER, Skills.ARCHITECT));
        library.addTeam(team3);
        String result = "Проект строительства закрыт";
        Assert.assertEquals(library.result(), result);
    }
}