package com.asoi.svintsitski.tender;

import org.junit.Assert;
import org.junit.Test;

public class TeamTest {

    @Test
    public void fullPrice() {
        Team team1 = new Team("Team 1");
        team1.addWorker(new Worker(3.7F, Skills.ENGINEER, Skills.ARCHITECT));
        team1.addWorker(new Worker(7F, Skills.ENGINEER));
        float price = 10.7F;
        Assert.assertEquals(team1.fullPrice(), price, 0.0001);
    }

    @Test
    public void teamName() {
        Team team1 = new Team("Team 1");
        String name = "Team 1";
        Assert.assertEquals(team1.teamName(), name);
    }
}