package com.asoi.svintsitski.tender;

public enum Skills {
    ARCHITECT,
    TECHNOLOGIST,
    ECONOMIST,
    ENGINEER,
    PAINTER
}
