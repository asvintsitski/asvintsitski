package com.asoi.svintsitski.tender;

import java.util.Collections;
import java.util.EnumSet;

class Worker {

    EnumSet<Skills> skills = EnumSet.noneOf(Skills.class);
    float price;

    Worker(float price, Skills... skills) {
        this.price = price;
        Collections.addAll(this.skills, skills);
    }
}
