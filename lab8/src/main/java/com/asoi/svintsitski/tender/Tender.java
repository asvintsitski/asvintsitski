package com.asoi.svintsitski.tender;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class Tender {
    private List<Team> teams = new ArrayList<>();
    private Map<Skills, Integer> requiredSkills = new HashMap<>();
    private List<Team> equalsTeams = new ArrayList<>();

    void addSkill(Skills skill, int count) {
        requiredSkills.put(skill, count);
    }

    void addTeam(Team team) {
        this.teams.add(team);
    }

    String result() {

        for (Team team : teams) {
            boolean check = true;
            Map<Skills, Integer> equalsSkills = new HashMap<>(requiredSkills);
            for (Worker worker : team.workers) {
                for (Skills skill : worker.skills) {
                    equalsSkills.put(skill, equalsSkills.get(skill) - 1);
                }
            }
            for (Map.Entry<Skills, Integer> entry : equalsSkills.entrySet()) {
                if (entry.getValue() > 0) {
                    check = false;
                }
            }
            if (check) {
                equalsTeams.add(team);
            }
        }
        if (equalsTeams.size() == 0) {
            return ("Проект строительства закрыт");
        } else {
            String teamName = equalsTeams.get(0).teamName();
            float fullPrice = equalsTeams.get(0).fullPrice();
            for (Team team : equalsTeams) {
                if (fullPrice > team.fullPrice()) {
                    fullPrice = team.fullPrice();
                    teamName = team.teamName();
                }
            }
            return ("Самая дешевая бригада: " + teamName + ". Ее цена: " + fullPrice + ".");
        }
    }
}
