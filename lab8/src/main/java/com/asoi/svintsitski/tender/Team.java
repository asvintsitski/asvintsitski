package com.asoi.svintsitski.tender;

import java.util.ArrayList;
import java.util.List;

class Team {
    private String name;
    List<Worker> workers = new ArrayList<>();

    void addWorker(Worker worker) {
        this.workers.add(worker);
    }

    Team(String name) {
        this.name = name;
    }

    float fullPrice() {
        float fullPrice = 0;
        for (Worker worker : workers) {
            fullPrice += worker.price;
        }
        return fullPrice;
    }

    String teamName() {
        return name;
    }
}
