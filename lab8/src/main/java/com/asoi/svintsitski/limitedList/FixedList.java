package com.asoi.svintsitski.limitedList;

public class FixedList<T> {
    private Object[] array ;
    private int pointer = 0;

    FixedList(int size){
        array = new Object[size];
    }

    public void add(T item) throws FixedListException {
        try{
            if(pointer == array.length)
                throw new FixedListException("Лист заполнен");
            array[pointer++] = item;
        }catch(Exception e){
            System.err.println(e.getMessage());
        }
    }

    public T get(int index) {
        return (T) array[index];
    }

    public void remove(int index) {
        for (int i = index; i<pointer; i++)
            array[i] = array[i+1];
        array[pointer] = null;
        pointer--;
    }

    public int size() {
        return pointer;
    }

    public void resize(int newLength) {
        Object[] newArray = new Object[newLength];
        System.arraycopy(array, 0, newArray, 0, pointer);
        array = newArray;
    }

    public void print() {
        for (int i = 0; i<pointer;i++){
            System.out.println((Object) array[i]);
        }
    }
}