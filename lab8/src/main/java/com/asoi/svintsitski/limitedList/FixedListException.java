package com.asoi.svintsitski.limitedList;

class FixedListException extends Exception{
    FixedListException(String message){
        super(message);
    }
}
